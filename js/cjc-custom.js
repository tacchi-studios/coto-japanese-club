// Convert SVG into inline
jQuery(function(){
    jQuery('img.svg').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, else we gonna set it if we can.
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');
    });
});

jQuery(function($) {
    $("select option:first").attr('disabled', 'disabled');// Disable the first value/label ---
});

// Testimonial carousel settings
jQuery(function($) {
  $owlContainer = $('#testimonials');
  $owlSlides    = $owlContainer.children('div');

  if ($owlSlides.length > 3) { // If there are more than 3 items
      $owlContainer.owlCarousel({
        loop:true,
        autoWidth:false,
        items:3,
        responsiveClass:true,
        responsive:{
          0:{items :1, stagePadding: 60},
          350:{items :1, stagePadding: 75},
          400:{items :1, stagePadding: 90},
          450:{items :1, stagePadding: 110},
          500:{items :1, stagePadding: 120},
          550:{items :1, stagePadding: 140},
          600:{items :1, stagePadding: 155},
          650:{items :1, stagePadding: 170},
          700:{items :1, stagePadding: 180},
          730:{items :1, stagePadding: 195},
          768:{items: 3},
        },
        dots: false,
        nav:true,
        navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>']
      });
  } else if ($owlSlides.length > 1 && $owlSlides.length <= 3 ) { // If there are between 2 to 3 items
      $owlContainer.owlCarousel({
        loop:true,
        autoWidth:false,
        items:3,
        responsiveClass:true,
        responsive:{
          0:{items :1, stagePadding: 60},
          350:{items :1, stagePadding: 75},
          400:{items :1, stagePadding: 90},
          450:{items :1, stagePadding: 110},
          500:{items :1, stagePadding: 120},
          550:{items :1, stagePadding: 140},
          600:{items :1, stagePadding: 155},
          650:{items :1, stagePadding: 170},
          700:{items :1, stagePadding: 180},
          730:{items :1, stagePadding: 195},
          768:{items: 3, touchDrag  : false, mouseDrag  : false, loop:false},
        },
        dots: false,
        nav:true,
        navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>']
      });
  } else {
    $owlContainer.owlCarousel({ // If there is only 1 item
      touchDrag  : false,
      mouseDrag  : false,
      nav:false,
    });
  }
});

// Custom offset for headroom
$(window).scroll(function() {
  if ($(window).width() < 620) {
    var scroll = $(window).scrollTop();
    if (scroll >= 300) {
      setTimeout(function() {
        $(".navbar.main").addClass("mobile-offset");
      }, 200)
    } else {
      $(".navbar.main").removeClass("mobile-offset");
    }
  }
});


// display other field if user selects "other"
jQuery(document).ready(function(){
   //execute code when document is ready
   jQuery('.note1').hide();
   jQuery(document).on("change", "#find-us", function () {
       jQuery('.note1').hide();
       var neededId = jQuery(this).val();
       var divToShow = jQuery(".note1").filter("[id='" + neededId + "']");
       divToShow.show();
   });
});

// If paragraph in content contains empty span, add class
jQuery(document).ready(function(){
  $('.content p > span:empty, .content h5:empty, .content h6:empty, .content h4:empty').closest('p, h5, h6, h4').addClass('empty');
});

