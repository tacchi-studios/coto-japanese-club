<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta name="format-detection" content="telephone=no">
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/img/favicons/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url'); ?>/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url'); ?>/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="<?php bloginfo('template_url'); ?>/img/favicons/manifest.json">
  <link rel="mask-icon" href="<?php bloginfo('template_url'); ?>/img/favicons/safari-pinned-tab.svg" color="#5bbad5">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.theme.default.min.css">

  <!-- Custom Scripts -->
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/headroom.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/owl.carousel.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/cjc-custom.js"></script>

  <!-- IE 10 only css fix -->
  <script>var doc = document.documentElement; doc.setAttribute('data-useragent', navigator.userAgent);</script>

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="hfeed site" id="page">

<header>
  <nav class="global-navbar">
    <?php wp_nav_menu(
      array(
        'menu'           => 'Coto Global Menu', // Global menus
      )
    ); ?>
  </nav>
  <nav class="navbar main">
    <div class="container">
      <a class="logo" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>">
        <img width="145.35" src="<?php bloginfo('stylesheet_directory'); ?>/img/logo.svg" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
      </a>
      <?php wp_nav_menu(
        array(
          'theme_location'  => 'primary',
          'fallback_cb'     => '',
          'menu_id'         => 'main-menu',
          'walker'          => new WP_Bootstrap_Navwalker(),
        )
      ); ?>
    </div>
  </nav>
</header>

<div class="nav-space"></div>

<script>var myElement = document.querySelector(".navbar.main");var headroom  = new Headroom(myElement);headroom.init();</script>
<script>var myElement = document.querySelector(".nav-space");var headroom  = new Headroom(myElement);headroom.init();</script>
