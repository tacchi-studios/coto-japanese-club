<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div id="content" tabindex="-1">
  <?php while ( have_posts() ) : the_post(); ?>
    <?php if ( is_singular('school_courses') ) {
      get_template_part( 'loop-templates/course-single' );
    } else {
      get_template_part( 'loop-templates/content', 'single' );
    }?>
  <?php endwhile; // end of the loop. ?>
</div>

<?php get_footer(); ?>

