<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_sidebar( 'footerfull' ); ?>

<footer class="site-footer bg-aqua">
    <img class="svg positive-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/positive-curve.svg">
  <div class="container">
    <ul class="site-info">
      <li class="logo-container">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
          <img class="logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-white.svg" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
        </a>
      </li>
      <li class="menu">
        <?php wp_nav_menu( //get header menu items
          array(
            'theme_location'  => 'primary',
            'fallback_cb'     => '',
            'walker'          => new WP_Bootstrap_Navwalker(),
          )
        ); ?>
      </li>

      <li class="contact-info-container txt-white">
        <div class="contact-info">
          <table>
            <?php $page = get_page_by_title( 'Contact' ); ?><!-- get ID from page name -->
            <tr>
              <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
              <td><p><?php the_field('email_address', $page); ?></p></td>
            </tr>
            <tr class="phone">
              <td><i class="fa fa-mobile" aria-hidden="true"></i></td>
              <td><p><?php the_field('phone_number', $page); ?></p></td>
            </tr>
            <tr>
              <td><i class="fa fa-building" aria-hidden="true"></i></td>
              <td><p><?php the_field('school_address', $page); ?><br><?php the_field('opening_hours', $page); ?></p></td>
            </tr>
          </table>
          <ul class="social-links">
            <?php if( get_field('facebook_link', $page) ) { ?>
              <li><a target="_blank" href="<?php the_field('facebook_link', $page); ?>"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
            <?php } ?>
            <?php if( get_field('twitter_link', $page) ) { ?>
              <li><a target="_blank" href="<?php the_field('twitter_link', $page); ?>"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
            <?php } ?>
            <?php if( get_field('instagram_link', $page) ) { ?>
              <li><a target="_blank" href="<?php the_field('instagram_link', $page); ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            <?php } ?>
          </ul>
        </div>
      </li>
      <li class="newsletter-signup txt-white">
        <p class="title"><b>Subscribe to Coto News</b></p><!-- get newsletter signup inquiry form -->
        <p>Receive announcements and information about courses, events, parties and more in our weekly newsletter.</p>
        <?php echo do_shortcode( '[contact-form-7 id="182" title="Newsletter Signup" html_class="news-letter-signup"]' ); ?>
      </li>
    </ul>
  </div>
</footer>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-34354900-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-34354900-3');
</script>

</body>

</html>


