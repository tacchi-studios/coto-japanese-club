<?php
/**
 * Template Name: Courses Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div id="content">
  <div class="banner sm">
    <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
    <div class="banner-img" style="background-image: url(<?php echo $backgroundImg[0]; ?>);"></div>
    <img class="desktop negative-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/banner-curve.svg">
    <img class="mobile negative-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/negative-curve-aqua.svg">
  </div>

  <div class="curve mobile bg-aqua">
    <img class="svg positive-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/positive-curve-mobile.svg">
  </div>



  <img class="curve-arrow-piece" src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-arrow-piece.svg">
  <main class="site-main" id="main" role="main">
    <section class="container max-1220 no-pad"><!-- Courses -->
      <div class="text-centered">
        <div class="mobile" style="height: 20px"></div>
        <h1 class="h2-size">Coto Club Courses</h1>
        <div class="desktop" style="height:20px"></div>
        <div class="mobile" style="height:0px"></div>
        <h2 class="subtitle txt-aqua f-weight-400"><?php the_field('catch_copy'); ?></h2>
      </div>
      <div class="desktop" style="height:80px"></div>
      <div class="mobile" style="height:30px"></div>
      <?php $loop = new WP_Query( array( 'post_type' => 'school_courses', 'posts_per_page' => -99 ) ); ?>
      <?php $the_count = $loop->found_posts; ?>
      <ul class="course-list no-center">
        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
          <?php get_template_part( 'loop-templates/course'); ?>
        <?php endwhile; wp_reset_query(); ?>
      </ul>
      <div class="desktop" style="height:100px"></div>
      <div class="mobile" style="height:40px"></div>
    </section>
    <section class="inquiry"><!-- Large inquiry form -->
      <img class="svg positive-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/positive-curve.svg">
      <div class="container max-780">
        <div class="text-centered">
          <h2 class="txt-velvet">Interested in our courses?</h2>
          <p class="txt-dark-grey h5-size">Get in touch and let us know how we can help you achieve your study goals. We'll invite you over for a tour of the school and a free level check.</p>
          <ul class="contact">
            <?php $page = get_page_by_title( 'Contact' ); ?><!-- get ID from page name -->
            <li><p><i class="fa fa-envelope" aria-hidden="true"></i><?php the_field('email_address', $page); ?></p><li>
            <li><p><i class="fa fa-mobile" aria-hidden="true"></i><?php the_field('phone_number', $page); ?></p><li>
          </ul>
        </div>
        <?php echo do_shortcode( '[contact-form-7 id="105" title="Large Inquiry Form"]' ); ?>
      </div>
    </section>
  </main>
</div>


<?php get_footer(); ?>


