<?php
/**
 * Template Name: Azabu Club Landing Page Template
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

?>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title"
      content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>

    <!-- font -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

  </head>
  <body>
    <main class="site-main" id="main" role="main">
        <section>
          <div class="container">
            <img class="logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/coto-club-logo.svg" alt="Coto Club Azabu Juban Logo">
            <div class="cta">
              <h1><?php the_field('main_title'); ?></h1>
              <p><?php the_field('main_description'); ?></p>
              <div class="form-container">
                <p>Receive announcements about trial lessons, opening parties, course start dates and more!</p>
                <?php echo do_shortcode( '[contact-form-7 id="56" title="Coto Club landing page email signup"]' ); ?>
              </div>
            </div>
              <div class="above-1600">
                <img
                  class="speech-bubble-image"
                  alt="Coto club azabu juban women smiling"
                  src="<?php bloginfo('stylesheet_directory'); ?>/img/women-speech-bubble.png"
                >
              </div>
              <div class="below-1600">
                <img
                  class="speech-bubble-image"
                  alt="Coto club azabu juban women smiling"
                  src="<?php bloginfo('stylesheet_directory'); ?>/img/women-speech-bubble-1600.png"
                >
              </div>
              <div class="below-767">
                <img
                  class="speech-bubble-image"
                  alt="Coto club azabu juban women smiling"
                  src="<?php bloginfo('stylesheet_directory'); ?>/img/women-speech-bubble-767.png"
                >
              </div>
          </div>
        </section>
        <article>
          <div class="container">
            <h2><?php the_field('section_title'); ?></h2>
            <ul>
              <li><p><?php the_field('section_description_paragraph_1'); ?></p></li>
              <li><p><?php the_field('section_description_paragraph_2'); ?></p></li>
            </ul>
          </div>
        </article>
        <section class="events">
          <div class="container">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/event-icon.svg" alt="Coto Club Azabu Events">
            <h2><?php the_field('announcement_title'); ?></h2>
            <p><?php the_field('announcement_description'); ?></p>
            <ul>
              <li>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/img/japanese-study-course.jpg" alt="japanese-study-course">
                <h4><?php the_field('event_title_1'); ?></h4>
                <p><?php the_field('event_description_1'); ?></p>
                <hr>
                <p class="time"><?php the_field('event_time_1'); ?></p>
              </li>
              <li>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/img/cafe-meetup.jpg" alt="cafe-meetup">
                <h4><?php the_field('event_title_2'); ?></h4>
                <p><?php the_field('event_description_2'); ?></p>
                <hr>
                <p class="time"><?php the_field('event_time_2'); ?></p>
              </li>
              <li>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/img/venue-icon.svg" alt="Coto Club Azabu Venue">
                <p>
                  <b>Venue</b><br>
                  <?php the_field('event_venue'); ?>
                </p>
                <a target="_blank" href="<?php the_field('google_maps_url'); ?>">Google maps</a>
                <p><?php the_field('phone_number'); ?><br><?php the_field('email_address'); ?></p>
              </li>
            </ul>
          </div>
        </section>
        <footer>
          <div class="container">
            <ul>
              <li>Coto Japanese Club</li>
              <li><address><?php the_field('event_venue'); ?></address></li>
              <li>Tel: <?php the_field('phone_number'); ?></li>
            </ul>
            <p>Copyright © Coto Japanese Club. All Rights Reserved</p>
          </div>
        </footer>
    </main>
  </body>

  <style>
    body {
      margin: 0;
    }
    @media (max-width: 1200px) {
      html {
        margin-top: 0!important;
      }
    }
    /* Global font family */
    h1, h2, p, ul > li, span, input, .form-container form > .wpcf7-mail-sent-ok{
      font-family: 'lato', sans-serif;
    }
    /* Global font weight */
    p {
      font-weight: 300;
      margin-top: 0;
    }
    /* font-size | color | line-height | font-weight | margin */
    h1 {
      margin-top: 0;
      font-size: 61px;
      color: black;
      line-height: 1;
      font-weight: 600;
      margin-bottom: 35px;
    }
    h2 {
      font-weight: 600;
      font-size: 32px;
      margin-top: 0;
    }
    h4 {
      font-size: 26px;
    }
    main {
      overflow: hidden;
    }
    section p {
      font-size: 22px;
      color: #999999;
      line-height: 1.22;
      margin-bottom: 40px;
    }
    section .form-container > p {
      color: #333333;
      font-weight: 400;
      margin-bottom: 20px;
    }
    article h2 {
      font-size: 32px;
      color: white;
      line-height: 1.12;
      margin-bottom: 30px;
    }
    article p {
      font-size: 18px;
      color: white;
      line-height: 1.3;
    }
    footer ul > li {
      font-size: 18px;
      color: #666666;
      font-weight: 300;
    }
    footer ul > li:first-child {
      color: black;
      font-weight: 400;
    }
    footer p {
      font-size: 18px;
      color: #999999;
    }
    @media (max-width: 1200px) {
      h1 {
        font-size: 50px;
      }
      section p {
        font-size: 18px;
      }
      footer p {
        font-size: 16px;
      }
      footer ul > li  {
        font-size: 16px;
      }
      article h2 {
        font-size: 26px;
      }
    }
    @media (max-width: 850px) {
      h1 {
        font-size: 40px;
        margin-bottom: 20px;
      }
      section p {
        font-size: 16px;
        margin-bottom: 20px;
      }
      article p {
        font-size: 18px;
      }
      footer p {
        font-size: 14px;
      }
      footer ul > li  {
        font-size: 14px;
      }
      article h2 {
        font-size: 24px;
      }
    }
    @media (max-width: 400px) {
      h1 {
        font-size: 36px;
        margin-bottom: 15px;
      }
      footer p {
        font-size: 14px;
      }
      article p {
        font-size: 18px;
      }
      article h2 {
        font-size: 24px;
        line-height:1.3;
      }
    }
    /* li */
    ul {
      list-style: none;
      padding: 0;
    }
    /* logo */
    img.logo {
      max-width: 253px;
      max-height: 127px;
      margin-left: -30px;
      margin-bottom: 40px;
    }
    @media (max-width: 1200px) {
      img.logo {
        max-width: 200px;
        max-height: 100px;
        margin-left: -20px;
        margin-bottom: 20px;
      }
    }
    @media (max-width: 850px) {
      img.logo {
        max-width: 160px;
        max-height: 80px;
        margin-left: -10px;
        margin-bottom: 10px;
      }
    }
    @media (max-width: 767px) {
      img.logo {
        display: inherit;
        margin: 0 auto;
        margin-bottom: 10px;
      }
    }
    @media (max-width: 400px) {
      img.logo {
        max-width: 140px;
        max-height: 70px;
      }
    }
    .speech-bubble-image {
      position: absolute;
      top: -33px;
      right: -150px;
      max-width: 854px;
    }
    @media (max-width: 1400px) {
      .speech-bubble-image {
        max-width: 58vw;
      }
    }
    @media (max-width: 1200px) {
      .speech-bubble-image {
        max-width: 62vw;
      }
    }
    @media (max-width: 920px) {
      .speech-bubble-image {
        max-width: 61vw;
        top: 0;
      }
    }
    @media (max-width: 850px) {
      .speech-bubble-image {
        right: -100px;
      }
    }
    @media (max-width: 767px) {
      .speech-bubble-image {
        position: relative;
        max-width: 400px;
        width: 100%;
        margin: 0 auto;
        display: block;
        right: 0;
        margin-top: 40px;
        margin-bottom: -73px;
      }
    }
    @media (max-width: 400px) {
      .speech-bubble-image {
        max-width: calc(100% + 80px);
        width: calc(100% + 80px);
        margin-left: -40px;
        position: relative;
        margin-bottom: -263px;
      }
    }
    .above-1600 .speech-bubble-image {
      max-width: 993px;
      right: -289px;
    }
    @media (min-width:1601px) {
      .above-1600 {
        display: block;
      }
      .below-1600,
      .below-767 {
        display: none;
      }
    }
    @media (max-width:1600px) {
      .above-1600,
      .below-767 {
        display: none;
      }
      .below-1600 {
        display: block;
      }
    }
    @media (max-width:767px) {
      .above-1600,
      .below-1600 {
        display: none;
      }
      .below-767 {
        display: block;
      }
    }
    .container {
      max-width: 1300px;
      width: calc(100% - 160px);
      margin: 0 auto;
      padding: 50px 0;
      position: relative;
    }
    @media (max-width: 1200px) {
      .container {
        padding: 40px 0;
        width: calc(100% - 120px);
      }
    }
    @media (max-width: 850px) {
      .container {
        padding: 30px 0;
        width: calc(100% - 80px);
      }
    }
    @media (max-width:767px){
      .container {
        width: calc(100% - 60px);
      }
    }
    @media (max-width:400px){
      .container {
        width: calc(100% - 40px);
        padding: 20px 0;
      }
    }

    /* cta section */
    .cta {
      max-width: 477px;
    }
    .cta > p {
      max-width: 427px;
    }
    @media (max-width: 1200px) {
      .cta {
        max-width: 371px;
      }
      .cta > p {
        max-width: 330px;
      }
    }
    @media (max-width: 850px) {
      .cta {
        max-width: 280px;
      }
      .cta > p {
        max-width: 280px;
      }
    }
    @media (max-width: 767px) {
      .cta {
        text-align: center;
        max-width: 400px;
        margin: 0 auto;
      }
      .cta > p {
        max-width: 100%;
      }
    }

    /* email submission form */
    .form-container form > p input[type=submit],
    .form-container form > p > span > input[type=email] {
      display: inline-block;
      line-height: 68px;
      height: 68px;
      border-radius: 0;
      padding: 0 20px;
      vertical-align: top;
      font-size: 22px;
      font-weight: 300;
    }
    .form-container form > p > span > input[type=email] {
      width: calc(100% - 130px);
      border:1px solid #CCCCCC;
      border-left: 4px solid #00BBB4;
    }
    .form-container form > p > span > input[type=email]:focus {
      border:1px solid #00BBB4;
      border-left: 4px solid #00BBB4;
    }
    .form-container form > p input[type=submit] {
      cursor: pointer;
      max-width: 120px;
      background-color: #00BBB4;
      color: White;
      border: 1px solid #00BBB4;
      border-top-right-radius: 6px;
      border-bottom-right-radius: 6px;
      margin-left: -6px;
    }
    .form-container form > p input[type=submit]:focus,
    .form-container form > p input[type=submit]:hover,
    .form-container form > p input[type=submit]:active {
      background-color: #019792;
      border: 1px solid #019792;
      color: White!important;
      outline: none!important;
    }
    .form-container form > p input[type=submit]:active,
    .form-container form > p input[type=submit]:focus {
      box-shadow: inset 0px 5px 50px 0px rgba(0,0,0,.2)!important;
      outline: none!important;
    }
    .form-container form p br {
      display: none;
    }
    .form-container form > p > span > span[role=alert]{
      top: calc(100% + 50px);
      position: absolute;
      font-size: 16px;
      color: #ff8186;
      text-align: left;
    }
    .form-container form > .wpcf7-validation-errors {
      display: none;
    }
    .form-container form > .wpcf7-mail-sent-ok {
      margin: 0;
      border: 0;
      padding: 0;
      color: white;
      position: absolute;
      top: 0;
      background: #00BBB4;
      line-height: 70px;
      height: 70px;
      width: calc(100% - 11px);
      border-radius: 6px;
      text-align: center;
      font-size: 22px;
      font-weight: 300;
    }
    .form-container form {
      position: relative;
    }
    @media (max-width: 1200px) {
      .form-container form > p input[type=submit],
      .form-container form > p > span > input[type=email] {
        line-height: 48px;
        height: 48px;
        font-size: 18px;
      }
      .form-container form > .wpcf7-mail-sent-ok {
        line-height: 50px;
        height: 50px;
        font-size: 16px;
      }
      .form-container form > p > span > span[role=alert] {
        width: calc(100% + 200px);
        top: calc(100% + 35px);
      }
    }
    @media (max-width: 850px) {
      .form-container form > p input[type=submit],
      .form-container form > p > span > input[type=email] {
        font-size: 16px;
        padding: 0 15px;
      }
      .form-container form > p input[type=submit] {
        max-width: 76px;

      }
      .form-container form > p > span > input[type=email] {
        width: calc(100% - 76px);
        border-left: 2px solid #00BBB4;
      }
      .form-container form > p > span > span[role=alert] {
        font-size: 14px;
      }
    }
    @media (max-width: 767px) {
      .form-container form > p > span > span[role=alert] {
        top: calc(100% + 35px);
        position: absolute;
        width: calc(100% + 100px);
        font-size: 14px;
      }
    }
    @media (max-width: 400px) {
      .form-container form > p input[type=submit],
      .form-container form > p > span > input[type=email] {
        line-height: 43px;
        height: 43px;
      }
      .form-container form > .wpcf7-mail-sent-ok {
        line-height: 45px;
        height: 45px;
        font-size: 16px;
      }
      .form-container form > p > span > span[role=alert] {
        top: calc(100% + 30px);
        position: absolute;
        width: calc(100% + 100px);
        margin-left: 0;
        font-size: 14px;
      }
    }

    /* article section */
    article {
      background-color: #00BBB4;
      padding: 25px 0;
    }
    article h2 {
      max-width: 798px;
    }
    article ul {
      max-width: 850px;
      margin: 0;
    }
    article ul > li {
      vertical-align: top;
      display: inline-block;
      width: calc(50% - 25px);
    }
    article ul > li:first-child {
      margin-right: 22px;
    }
    article ul > li:last-child {
      margin-left: 22px;
    }
    @media (max-width: 1200px) {
      article {
        padding: 15px 0;
      }
      article h2 {
        max-width: 600px;
      }
      article ul {
        max-width: 700px;
        margin: 0;
      }
      article ul > li {
        width: calc(50% - 15px);
      }
      article ul > li:first-child {
        margin-right: 11px;
      }
      article ul > li:last-child {
        margin-left: 11px;
      }
    }
    @media (max-width: 850px) {
      article h2 {
        max-width: 470px;
      }
      article ul {
        max-width: 650px;
        margin: 0;
      }
    }
    @media (max-width: 767px) {
      article {
        padding-top: 30px;
        padding-bottom: 0px;
      }
      article h2 {
        max-width: 100%;
      }
      article ul {
        max-width: 100%;
      }
      article ul > li {
        width: 100%;
        margin: 0!important;
      }
    }
    @media (max-width: 400px) {
      article {
        padding-top: 245px;
        padding-bottom: 30px;
      }
    }
    /* events section */
    section.events {
      padding:25px 0;
    }
    section.events h2 {
      margin-bottom: 15px;
      display: inline-block;
      vertical-align: top;
      margin-left: 7px;
      margin-top: -3px;
    }
    section.events > .container > p {
      max-width: 725px;
    }
    section.events > .container > img {
      width: 31px;
      height: 29px;
    }
    section.events ul {
      display: -webkit-box;
      display: -moz-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
    }
    section.events ul > li {
      border: 1px solid #D4D4D4;
      padding: 20px;
      border-radius: 6px;
      width: calc(33.333333% - 25px);
      margin-right: 25px;
      text-align: center;
      overflow: hidden;
    }
    section.events ul > li > img:first-child {
      max-width: calc(100% + 40px);
      display: block;
      margin-left: -20px;
      margin-top: -21px;
    }

    section.events ul > li:last-child {
      width: 200px;
      border: none;
      text-align: left;
      padding: 0;
      padding-left: 20px;
    }
    section.events ul > li > h4 {
      margin-top: 25px;
      margin-bottom: 15px;
      font-weight: 600;
      color: #00BBB4;
    }
    section.events ul > li > p {
      font-size: 18px;
      color: #333333;
      max-width: 290px;
      margin: 0 auto;
      font-weight: 400;
      line-height: 1.4;
    }
    section.events ul > li > hr {
      border-top: 4px solid #00BBB4;
      max-width: 60px;
      margin: 37px auto;
    }
    section.events ul > li > p.time {
      font-size: 16px;
      color: #9A9A9A;
      max-width: 100%;
      margin-bottom: 5px;
    }
    section.events ul > li:last-child > img {
      margin: 0;
      margin-bottom: 10px;
      width: 34px;
      height: 27px;
    }
    section.events ul > li:last-child > a {
      color: #00BBB4;
      border: 2px solid #00BBB4;
      border-radius: 6px;
      font-weight: 600;
      padding: 4px 10px;
      display:inline-block;
      text-decoration: none;
      margin-top: 14px;
      margin-bottom: 27px;
      transition: .14s all;
      -webkit-transition: .14s all;
      -moz-transition: .14s all;
      -ms-transition: .14s all;
      -o-transition: .14s all;
    }
    section.events ul > li:last-child > a:hover,
    section.events ul > li:last-child > a:focus {
      color: #ffffff;
      background-color: #00BBB4;
    }
    section.events ul > li:last-child > a:active {
      box-shadow: inset 0px 5px 50px 0px rgba(0,0,0,.2)!important;
    }
    @media (max-width: 1200px) {
      section.events ul > li > h4 {
        font-size: 20px;
        margin-bottom: 10px;
      }
      section.events ul > li > p {
        font-size: 16px;
        width: 215px;
      }
      section.events ul > li > p.time {
        font-size: 15px;
        width: 206px;
      }
      section.events ul > li {
        margin-right: 15px;
        padding: 15px;
        width: calc(40% - 25px);
      }
      section.events ul > li > hr {
        margin: 15px auto;
        border-top: 2px solid #00BBB4;
      }
      section.events ul > li:last-child {
        width: 178px;
      }
      section.events ul > li:last-child > a {
        margin-top: 10px;
        margin-bottom: 20px;
      }
      section.events ul > li:last-child > p {
        width: 100%;
      }
    }
    @media (max-width: 825px) {
      section.events h2 {
        max-width: 364px;
        margin: 0 auto;
        margin-bottom: 15px;
        display: block;
        text-align: center;
      }
      section.events > .container > p {
        max-width: 600px;
        font-size: 18px;
        width: 100%;
        text-align: center;
        margin: 0 auto;
        margin-bottom: 50px;
      }
      section.events > .container > img {
        display: block;
        text-align: center;
        width: 100%;
        height: 40px;
        margin-bottom: 15px;
      }
      section.events ul > li > h4 {
        font-size: 26px;
      }
      section.events ul > li > p {
        width: 100%;
      }
      section.events ul > li > p.time {
        width: 100%;
      }
      section.events ul {
        display: block;
        text-align: center;
      }
      section.events ul > li {
        margin-right: 0;
        margin-bottom: 20px;
        max-width: 100%;
        width: calc(100% - 30px);
      }
      section.events ul > li:last-child {
        width: 236px;
        padding: 0;
        margin: 0 auto;
        margin-top: 20px;
        text-align: center;

      }
      section.events ul > li:last-child > p {
        width: 100%;
        text-align: center;
      }
      section.events ul > li:last-child > a {
        text-align: center;
      }
      section.events ul > li:last-child > img {
        width: 100%;
        height: 40px;
      }
    }
    @media (max-width: 400px) {
      section.events h2 {
        font-size: 24px;
      }
    }
    @media (max-width: 370px) {
      section.events > .container > p {
        margin-bottom: 30px;
      }
    }

    /* footer section */
    footer {
      text-align: center;
    }
    footer > .container {
      border-top: 1px solid #E8E8E8;
    }
    footer ul {
      margin-bottom: 0;
    }
    footer ul > li {
      display: inline-block;
      margin: 0 8px;
    }
    footer ul > li > address {
      margin: 0;
      font-style:normal!important;
    }
    footer p {
      margin-top: 10px;
      margin-bottom: 0;
    }

  </style>

</html>