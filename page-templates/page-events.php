<?php
/**
 * Template Name: Events Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>


<div id="content">
  <div class="banner sm">
    <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
    <div class="banner-img" style="background-image: url(<?php echo $backgroundImg[0]; ?>);"></div>
    <img class="desktop negative-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/banner-curve.svg">
    <img class="mobile negative-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/negative-curve-aqua.svg">
  </div>

  <div class="curve mobile bg-aqua">
    <img class="svg positive-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/positive-curve-mobile.svg">
  </div>
  <img class="curve-arrow-piece" src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-arrow-piece.svg">
  <main class="site-main" id="main" role="main">
    <section class="container max-840 events no-pad" id="main" role="main"><!-- Events list -->
      <div class="text-centered">
        <div class="mobile" style="height: 20px"></div>
        <h1 class="h2-size"><?php the_field('catch_title'); ?></h1>
        <p class="h5-size txt-dark-grey"><?php the_field('catch_copy'); ?></p>
        <div class="desktop" style="height: 30px"></div>
        <div class="mobile" style="height: 10px"></div>
        <hr>
        <div class="desktop" style="height: 30px"></div>
        <div class="mobile" style="height: 10px"></div>
      </div>
      <?php $loop = new WP_Query( array( 'post_type' => 'school_events', 'posts_per_page' => -99 ) ); ?>
      <ul>
        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
          <?php get_template_part( 'loop-templates/event'); ?>
        <?php endwhile; wp_reset_query(); ?>
      </ul>
      <div class="desktop" style="height: 60px"></div>
      <div class="mobile" style="height: 10px"></div>
    </section>
    <section class="bg-velvet"><!-- Courses -->
      <div class="container text-centered">
        <h2 class="h2-size txt-white">Want to take your Japanese to new heights?</h2>
        <div style="height:10px"></div>
        <p class="h5-size txt-white">We have a range of courses to suit all study goals.</p>
      </div>
      <div class="desktop" style="height: 40px"></div>
      <div class="mobile" style="height: 20px"></div>
      <div class="container max-1220">
        <?php $loop = new WP_Query( array( 'post_type' => 'school_courses', 'posts_per_page' => 3 ) ); ?>
        <?php $the_count = $loop->found_posts; ?>
        <ul class="course-list">
          <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <?php get_template_part( 'loop-templates/course'); ?>
          <?php endwhile; wp_reset_query(); ?>
        </ul>
        <?php if ( ( $loop->have_posts() ) && ( $the_count > 3 ) ) : ?>
         <a class="more-link h5-size text-centered f-weight-400" href="<?php echo esc_url( home_url( '/courses' ) ); ?>">See all courses</a>
        <?php endif ?>
      </div>
    </section>
  </main>
</div>

<?php get_footer(); ?>



