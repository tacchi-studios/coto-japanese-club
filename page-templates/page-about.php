<?php
/**
 * Template Name: About Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>


<div id="content">
  <div class="banner sm">
    <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
    <div class="banner-img" style="background-image: url(<?php echo $backgroundImg[0]; ?>);"></div>
    <img class="desktop negative-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/banner-curve.svg">
    <img class="mobile negative-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/negative-curve-aqua.svg">
  </div>

  <div class="curve mobile bg-aqua">
    <img class="svg positive-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/positive-curve-mobile.svg">
  </div>
  <img class="curve-arrow-piece" src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-arrow-piece.svg">
  <main class="site-main" id="main" role="main">

    <section class="container max-1220 no-pad"><!-- Courses -->
      <div class="text-centered">
        <div class="mobile" style="height: 20px"></div>
        <h1 class="h2-size">Coto Japanese Club</h1>
        <div class="desktop" style="height:20px"></div>
        <div class="mobile" style="height:0px"></div>
        <h2 class="subtitle txt-aqua f-weight-400"><?php the_field('catch_copy'); ?></h2>
      </div>
      <div class="desktop" style="height:60px"></div>
      <div class="mobile" style="height:30px"></div>
    </section>

    <section class="container max-1220 jump-join"><!-- Jump in / join in section -->

      <div class="row jump">
        <div class="col-md-6">
          <div class="image-container bg-aqua">
            <img src="<?php the_field('jump_in_image'); ?>" alt="<?php the_field('jump_in_title'); ?>" title="<?php the_field('jump_in_title'); ?>" >
          </div>
        </div>
        <div class="col-md-6">
          <div class="text">
            <h3>
              Jump in<br>
              <span class="f-weight-400"><?php the_field('jump_in_title'); ?></span>
            </h3>
            <hr class="aqua long">
            <p class="h5-size f-weight-400 txt-dark-grey"><?php the_field('jump_in_description'); ?></p>
          </div>
        </div>
      </div>

      <div class="desktop" style="height: 50px;"></div>
      <div class="mobile" style="height: 60px;"></div>

      <div class="row join">
        <div class="col-md-6">
          <div class="image-container bg-aqua">
            <img src="<?php the_field('join_in_image'); ?>" alt="<?php the_field('join_in_title'); ?>" title="<?php the_field('jump_in_title'); ?>" >
          </div>
        </div>
        <div class="col-md-6">
          <div class="text">
            <h3>
              Join in<br>
              <span class="f-weight-400"><?php the_field('join_in_title'); ?></span>
            </h3>
            <hr class="aqua long">
            <p class="h5-size f-weight-400 txt-dark-grey"><?php the_field('join_in_description'); ?></p>
          </div>
        </div>
      </div>

      <div class="desktop" style="height: 50px;"></div>
      <div class="mobile" style="height: 30px;"></div>

    </section>

    <div class="desktop" style="height: 40px"></div>
    <div class="mobile" style="height:0px"></div>

    <section class="gallery"><!-- Coto Japanese club image gallery (4) -->
      <img class="svg" src="<?php bloginfo('stylesheet_directory'); ?>/img/negative-curve-small.svg">
      <ul>
        <li>
          <div class="image-container one">
            <?php $image = wp_get_attachment_image_src(get_field('cjc_gallery_image_1'), 'full'); ?>
            <div class="img" style="background-image: url(<?php echo $image[0]; ?>);" title="<?php echo get_the_title(get_field('cjc_gallery_image_1')) ?>" alt="<?php echo get_the_title(get_field('cjc_gallery_image_1')) ?>"></div>
          </div>
        </li>
        <li>
          <div class="image-container two">
            <?php $image = wp_get_attachment_image_src(get_field('cjc_gallery_image_2'), 'full'); ?>
            <div class="img" style="background-image: url(<?php echo $image[0]; ?>);" title="<?php echo get_the_title(get_field('cjc_gallery_image_2')) ?>" alt="<?php echo get_the_title(get_field('cjc_gallery_image_2')) ?>"></div>
          </div>
          <div class="image-container three">
            <?php $image = wp_get_attachment_image_src(get_field('cjc_gallery_image_3'), 'full'); ?>
            <div class="img" style="background-image: url(<?php echo $image[0]; ?>);" title="<?php echo get_the_title(get_field('cjc_gallery_image_3')) ?>" alt="<?php echo get_the_title(get_field('cjc_gallery_image_3')) ?>"></div>
          </div>
        </li>
        <li>
          <div class="image-container four">
            <?php $image = wp_get_attachment_image_src(get_field('cjc_gallery_image_4'), 'full'); ?>
            <div class="img" style="background-image: url(<?php echo $image[0]; ?>);" title="<?php echo get_the_title(get_field('cjc_gallery_image_4')) ?>" alt="<?php echo get_the_title(get_field('cjc_gallery_image_4')) ?>"></div>
          </div>
        </li>
      </ul>
    </section>

    <div class="desktop" style="height: 30px"></div>
    <div class="mobile" style="height: 0px"></div>

    <section class="contact text-centered">
      <div class="container">
        <h3>A convenient, accessible location in the heart of Azabu-Juban</h3>
        <div style="height: 3px"></div>
        <hr class="aqua long">
        <div style="height: 5px"></div>
        <p class="h5-size txt-dark-grey f-weight-400">Just a short stroll from Azabu-Juban station, Coto Club is a place you can drop into anytime.</p>
      </div>
      <div class="desktop" style="height: 50px"></div>
      <div class="mobile" style="height: 30px"></div>
      <iframe width="100%" height="200%" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=coto%20japanese%20club&key=AIzaSyBIxbiVeKtrXly1YING3F_qS_ZC_NHz2AE" allowfullscreen></iframe>

      <section class="contact container max-720"><!-- map and metro map -->
        <img src="<?php bloginfo('stylesheet_directory'); ?>/img/tokyo-metro-subway-map-to-coto-japanese-club-azabu-juban.png" alt="tokyo-metro-subway-map-to-coto-japanese-club-azabu-juban" title="tokyo-metro-subway-map-to-coto-japanese-club-azabu-juban">
      </section>
    </section>

    <section class="cla bg-pale-grey"><!-- About Coto Language academy -->
      <img class="svg positive-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/positive-curve.svg">

      <div class="desktop" style="height: 20px"></div>
      <div class="mobile" style="height: 10px"></div>

      <div class="container max-1110">
        <div class="row">
          <div class="col-md-5">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/coto-language-academy-logo.png" alt="coto-language-academy-logo" title="coto-language-academy-logo">
          </div>
          <div class="col-md-7">
            <h4><?php the_field('cla_title'); ?></h4>
            <hr class="cla long">
            <p class="h6-size txt-dark-grey f-weight-400"><?php the_field('cla_description'); ?></p>
          </div>
        </div>
        <div class="desktop" style="height: 40px"></div>
        <div class="mobile" style="height: 0px"></div>
      </div>
    </section>

    <section class="gallery bg-pale-grey"><!-- Coto Language academy image gallery (4) -->
      <img class="svg" src="<?php bloginfo('stylesheet_directory'); ?>/img/negative-curve-small.svg">
      <ul>
        <li>
          <div class="image-container one">
            <?php $image = wp_get_attachment_image_src(get_field('cla_gallery_image'), 'full'); ?>
            <div class="img" style="background-image: url(<?php echo $image[0]; ?>);" title="<?php echo get_the_title(get_field('cla_gallery_image')) ?>" alt="<?php echo get_the_title(get_field('cla_gallery_image')) ?>"></div>
          </div>
        </li>
        <li>
          <div class="image-container two">
            <?php $image = wp_get_attachment_image_src(get_field('cla_gallery_image_2'), 'full'); ?>
            <div class="img" style="background-image: url(<?php echo $image[0]; ?>);" title="<?php echo get_the_title(get_field('cla_gallery_image_2')) ?>" alt="<?php echo get_the_title(get_field('cla_gallery_image_2')) ?>"></div>
          </div>
          <div class="image-container three">
            <?php $image = wp_get_attachment_image_src(get_field('cla_gallery_image_3'), 'full'); ?>
            <div class="img" style="background-image: url(<?php echo $image[0]; ?>);" title="<?php echo get_the_title(get_field('cla_gallery_image_3')) ?>" alt="<?php echo get_the_title(get_field('cla_gallery_image_3')) ?>"></div>
          </div>
        </li>
        <li>
          <div class="image-container four">
            <?php $image = wp_get_attachment_image_src(get_field('cla_gallery_image_4'), 'full'); ?>
            <div class="img" style="background-image: url(<?php echo $image[0]; ?>);" title="<?php echo get_the_title(get_field('cla_gallery_image_4')) ?>" alt="<?php echo get_the_title(get_field('cla_gallery_image_4')) ?>"></div>
          </div>
        </li>
      </ul>
    </section>

    <section class="bg-velvet"><!-- Courses -->
      <div class="container text-centered">
        <h2 class="h2-size txt-white">Want to take your Japanese to new heights?</h2>
        <div style="height:10px"></div>
        <p class="h5-size txt-white">We have a range of courses to suit all study goals.</p>
      </div>
      <div class="desktop" style="height: 40px"></div>
      <div class="mobile" style="height: 20px"></div>
      <div class="container max-1220">
        <?php $loop = new WP_Query( array( 'post_type' => 'school_courses', 'posts_per_page' => 3 ) ); ?>
        <?php $the_count = $loop->found_posts; ?>
        <ul class="course-list">
          <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <?php get_template_part( 'loop-templates/course'); ?>
          <?php endwhile; wp_reset_query(); ?>
        </ul>
        <?php if ( ( $loop->have_posts() ) && ( $the_count > 3 ) ) : ?>
         <a class="more-link h5-size text-centered f-weight-400" href="<?php echo esc_url( home_url( '/courses' ) ); ?>">See all courses</a>
        <?php endif ?>
      </div>
    </section>

  </main>
</div>



<?php get_footer(); ?>

