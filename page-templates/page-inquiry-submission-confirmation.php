<?php
/**
 * Template Name: Inquiry submission confirmation Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div id="content">

<div class="mobile" style="height: 40px"></div>
<div class="no-banner-curve desktop">
  <img class="positive-curve no-banner" src="<?php bloginfo('stylesheet_directory'); ?>/img/position-curve-no-banner.svg">
  <img class="curve-arrow-piece" src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-arrow-piece.svg">
</div>

<main>
  <div class="container max-780"><!-- title -->
    <div class="container text-centered">
      <h2><?php the_field('catch_title'); ?></h2>
      <div style="height:20px;"></div>
      <p class="txt-dark-grey h6-size f-weight-400"><?php the_field('catch_copy'); ?></p>
      <div class="desktop" style="height: 20px"></div>
      <div class="mobile" style="height: 10px"></div>
    </div>
    <hr>
  </div>
  <div class="desktop" style="height: 50px"></div>
  <div class="mobile" style="height: 30px"></div>
  <section class="contact container max-780 no-pad"><!-- Contacts -->
    <div class="row" style="max-width: 600px; margin: 0 auto;">
      <div class="col-md-5">
        <table>
          <?php $page = get_page_by_title( 'Contact' ); ?><!-- get ID from page name -->
          <tr>
            <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
            <td><p><?php the_field('email_address', $page); ?></p></td>
          </tr>
          <tr class="phone">
            <td><i class="fa fa-mobile" aria-hidden="true"></i></td>
            <td><p><?php the_field('phone_number', $page); ?></p></td>
          </tr>
        </table>
      </div>
      <div class="col-md-7">
        <table>
          <tr>
            <td><i class="fa fa-building" aria-hidden="true"></i></td>
            <td><p><?php the_field('school_address', $page); ?><br><span class="txt-grey"><?php the_field('opening_hours', $page); ?></span></p></td>
          </tr>
        </table>
      </div>
    </div>
  </section>
  <div class="desktop" style="height: 120px"></div>
  <div class="mobile" style="height: 60px"></div>
</main>

</div>
<?php get_footer(); ?>
