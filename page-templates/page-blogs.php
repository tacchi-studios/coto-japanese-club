<?php
/**
 * Template Name: Blogs Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="banner sm">
  <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
  <div class="banner-img" style="background-image: url(<?php echo $backgroundImg[0]; ?>);"></div>
  <img class="desktop negative-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/banner-curve.svg">
  <img class="mobile negative-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/negative-curve-aqua.svg">
</div>

<div class="curve mobile bg-aqua">
  <img class="svg positive-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/positive-curve-mobile.svg">
</div>

<img class="curve-arrow-piece" src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-arrow-piece.svg">
<main class="site-main" id="main" role="main">

  <section class="blog-list container max-720 no-pad"><!-- blog list -->
    <div class="text-centered">
      <div class="mobile" style="height: 20px"></div>
      <h1 class="h2-size"><?php the_field('catch_title'); ?></h1>
      <h5><?php the_field('catch_copy'); ?></h5>
      <div class="desktop" style="height: 30px"></div>
      <div class="mobile" style="height: 10px"></div>
      <hr>
      <div class="desktop" style="height: 30px"></div>
      <div class="mobile" style="height: 10px"></div>
    </div>
    <ul>
      <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; $original_query = $wp_query; $wp_query = null; $args=array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page' => get_option( 'posts_per_page' ), 'paged'=>$paged); $wp_query = new WP_Query( $args );?>
      <?php if ( $wp_query->have_posts() ) : ?>
        <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
          <li>
            <div class="top-space" style="height: 5px"></div>
            <hr>
            <div class="top-space" style="height: 30px"></div>
            <p class="date f-weight-400"><?php echo get_the_date(); ?></p>
            <a href="<?php the_permalink(); ?>">
              <h4><?php the_title(); ?></h4>
            </a>
            <div class="desktop" style="height: 15px"></div>
            <div class="mobile" style="height: 10px"></div>
            <a href="<?php the_permalink(); ?>">
              <div class="banner">
                <?php if ( has_post_thumbnail() ) { ?>
                  <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );?>
                  <div class="banner-img" style="background-image: url(<?php echo $backgroundImg[0]; ?>);"></div>
                <?php } else { ?>
                  <div class="banner-img" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/img/coto-japanese-club-blog-placeholder-image.jpg);" alt="coto-japanese-club-blog"></div>
                <?php } ?>
              </div>
            </a>
            <div class="desktop" style="height: 20px"></div>
            <div class="mobile" style="height: 15px"></div>
            <p class="h6-size txt-dark-grey f-weight-400">
              <?php $content = get_the_content(); echo wp_strip_all_tags( mb_strimwidth($content, 0, 250, '...') ); ?>
              <a class="h6-size f-weight-400" href="<?php the_permalink() ?>">read more</a>
            </p>
          </li>
        <?php endwhile; ?>
          <?php wpbeginner_numeric_posts_nav(); ?>
        <?php endif; $wp_query = null; $wp_query = $original_query; wp_reset_postdata(); ?>
    </ul>
    <div class="desktop" style="height: 100px"></div>
    <div class="mobile" style="height: 50px"></div>
  </section>

  <section class="bg-velvet"><!-- Courses -->
    <div class="container text-centered">
      <h2 class="h2-size txt-white">Want to take your Japanese to new heights?</h2>
      <div style="height:10px"></div>
      <p class="h5-size txt-white">We have a range of courses to suit all study goals.</p>
    </div>
    <div class="desktop" style="height: 40px"></div>
    <div class="mobile" style="height: 20px"></div>
    <div class="container max-1220">
      <?php $loop = new WP_Query( array( 'post_type' => 'school_courses', 'posts_per_page' => 3 ) ); ?>
      <?php $the_count = $loop->found_posts; ?>
      <ul class="course-list">
        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
          <?php get_template_part( 'loop-templates/course'); ?>
        <?php endwhile; wp_reset_query(); ?>
      </ul>
      <?php if ( ( $loop->have_posts() ) && ( $the_count > 3 ) ) : ?>
       <a class="more-link h5-size text-centered f-weight-400" href="<?php echo esc_url( home_url( '/courses' ) ); ?>">See all courses</a>
      <?php endif ?>
    </div>
  </section>
</main>


<?php get_footer(); ?>


