<?php
/**
 * Template Name: Home Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="banner home">
  <div class="text-box">
    <div class="desktop container">
      <h1 class="txt-white"><?php the_field('hero_banner_title'); ?></h1>
      <h5 class="txt-white"><?php the_field('hero_banner_description'); ?></h5>
    </div>
  </div>
  <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
  <div class="banner-img" style="background-image: url(<?php echo $backgroundImg[0]; ?>);"></div>
  <img class="desktop negative-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/banner-curve.svg">
  <img class="mobile negative-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/negative-curve-aqua.svg">
</div>

<section class="mobile bg-aqua">
  <div class="container text-centered">
    <h1 class="txt-white"><?php the_field('hero_banner_title'); ?></h1>
    <h5 class="txt-white"><?php the_field('hero_banner_description'); ?></h5>
  </div>
  <img class="svg positive-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/positive-curve-mobile.svg">
</section>


<span>
  <img class="home curve-arrow-piece" src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-arrow-piece.svg">
</span>

<div id="content">

  <main class="site-main" id="main" role="main">

    <section class="container max-1220"><!-- Courses -->
        <div class="mobile" style="height:30px"></div>
        <h2 class="subtitle txt-aqua f-weight-400 text-centered"><?php the_field('catch_copy'); ?></h2>
      <div class="desktop" style="height:60px"></div>
      <div class="mobile" style="height:30px"></div>
        <?php $loop = new WP_Query( array( 'post_type' => 'school_courses', 'posts_per_page' => 3 ) ); ?>
        <?php $the_count = $loop->found_posts; ?>
        <ul class="course-list">
          <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <?php get_template_part( 'loop-templates/course'); ?>
          <?php endwhile; wp_reset_query(); ?>
        </ul>
        <?php if ( ( $loop->have_posts() ) && ( $the_count > 3 ) ) : ?>
         <a class="more-link h5-size text-centered f-weight-400" href="<?php echo esc_url( home_url( '/courses' ) ); ?>">See all courses</a>
        <?php endif ?>
      <div class="desktop" style="height:120px"></div>
      <div class="mobile" style="height:60px"></div>
    </section>

    <section class="promotion bg-velvet"><!-- Blog post promotion -->
      <div class="label">
        <img class="graphic" src="<?php bloginfo('stylesheet_directory'); ?>/img/coto-cartoon-graphic-2.svg">
        <img class="speech-bubble" src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-speech-bubble-white.svg">
        <p class="txt-velvet f-weight-400"><i>from the<br>blog</i></p>
      </div>
      <img class="svg positive-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/positive-curve.svg">
      <?php $post_object = get_field('promote_blog_post'); // Show selected event detail
      if( $post_object ):
        $post = $post_object;
        setup_postdata( $post );?>
          <div class="row container max-1110">
            <div class="col-md-6 vcenter">
              <a href="<?php the_permalink(); ?>">
                <div class="banner">
                  <?php if ( has_post_thumbnail() ) { ?>
                    <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );?>
                    <div class="banner-img" style="background-image: url(<?php echo $backgroundImg[0]; ?>);"></div>
                  <?php } else { ?>
                    <div class="banner-img" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/img/coto-japanese-club-blog-placeholder-image.jpg);" alt="coto-japanese-club-blog"></div>
                  <?php } ?>
                </div>
              </a>
            </div>
            <div class="col-md-6 vcenter">
              <div>
                <p class="date txt-white"><?php echo get_the_date(); ?></p>
                <h4 class="txt-white"><?php the_title(); ?></h4>
                <div style="height:10px;"></div>
                <p class="summary h6-size txt-white">
                  <?php $content = get_the_content(); echo wp_strip_all_tags( mb_strimwidth($content, 0, 200, '...') ); ?>
                </p>
                <a class="h6-size" href="<?php the_permalink() ?>">Read more on our blog</a>
              </div>
            </div>
          </div>
         <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
      <?php endif; ?>
    </section>

    <div style="height:40px"></div>

    <section class="testimonial"><!-- Testimonials -->
      <div class="container max-780 text-centered">
        <h2 class="txt-velvet"><b>Whats our students say about Coto</b></h2>
        <div style="height:20px;"></div>
        <p class="txt-dark-grey h5-size">With small class sizes and friendly ongoing support, our instructors work hard to make your lessons fun and results-oriented.</p>
      </div>
      <div class="container max-1024 testimonials-container">
          <div id="testimonials" class="owl-carousel">
            <?php $loop = new WP_Query( array( 'post_type' => 'testimonials', 'posts_per_page' => -99 ) ); ?>
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
              <?php get_template_part( 'loop-templates/testimonial'); ?>
            <?php endwhile; wp_reset_query(); ?>
          </div>
      </div>
    </section>

    <script>
    $(document).ready(function(){
      $(".owl-carousel").owlCarousel();
    });
    </script>


    <section class="inquiry"><!-- Large inquiry form -->
      <img class="svg positive-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/positive-curve.svg">
      <div class="container max-780">
        <div class="text-centered">
          <h2 class="txt-velvet">We'd love to hear from you!</h2>
          <p class="txt-dark-grey h5-size">Get in touch and let us know how we can help you achieve your study goals. We'll invite you over for a tour of the school and a free level check.</p>
          <ul class="contact">
            <?php $page = get_page_by_title( 'Contact' ); ?><!-- get ID from page name -->
            <li><p><i class="fa fa-envelope" aria-hidden="true"></i><?php the_field('email_address', $page); ?></p><li>
            <li><p><i class="fa fa-mobile" aria-hidden="true"></i><?php the_field('phone_number', $page); ?></p><li>
          </ul>
        </div>
        <?php echo do_shortcode( '[contact-form-7 id="105" title="Large Inquiry Form"]' ); ?>
      </div>
    </section>

  </main>
</div>

<?php get_footer(); ?>






