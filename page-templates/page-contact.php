<?php
/**
 * Template Name: Contact Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>


<div id="content">
  <div class="banner sm">
    <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
    <div class="banner-img" style="background-image: url(<?php echo $backgroundImg[0]; ?>);"></div>
    <img class="desktop negative-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/banner-curve.svg">
    <img class="mobile negative-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/negative-curve-aqua.svg">
  </div>

  <div class="curve mobile bg-aqua">
    <img class="svg positive-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/positive-curve-mobile.svg">
  </div>
  <img class="curve-arrow-piece" src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-arrow-piece.svg">
  <main class="site-main" id="main" role="main">

    <section class="container max-840 contact no-pad"><!-- Contacts -->
      <div class="text-centered">
        <div class="mobile" style="height: 20px"></div>
        <h1 class="h2-size">Contact Us</h1>
        <h5><?php the_field('catch_copy'); ?></h5>
        <div class="desktop" style="height: 30px"></div>
        <div class="mobile" style="height: 10px"></div>
        <hr>
        <div class="desktop" style="height: 30px"></div>
        <div class="mobile" style="height: 10px"></div>
      </div>
      <div class="row">
        <div class="col-md-5 col-sm-5 col-xs-5">
          <table>
            <tr>
              <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
              <td><p><?php the_field('email_address'); ?></p></td>
            </tr>
            <tr class="phone">
              <td><i class="fa fa-mobile" aria-hidden="true"></i></td>
              <td><p><?php the_field('phone_number'); ?></p></td>
            </tr>
            <tr>
              <td><i class="fa fa-building" aria-hidden="true"></i></td>
              <td><p><?php the_field('school_address'); ?><br><span class="txt-grey"><?php the_field('opening_hours'); ?></span></p></td>
            </tr>
          </table>
        </div>
        <div class="col-md-7 col-sm-7 col-xs-7">
          <table>
            <tr>
              <td><i class="fa fa-train" aria-hidden="true"></i></td>
              <td><p><?php the_field('direction_to_school'); ?></p></td>
            </tr>
          </table>
          <table>
            <tr>
              <td class="direction"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-of-tokyo-metro-namboku-line.png" alt="logo-of-tokyo-metro-namboku-line" title="logo-of-tokyo-metro-namboku-line"></td>
              <td>
                <p>
                  Tokyo Metro Namboku Line<br>
                  <span class="txt-grey">9 minutes from Megro</span>
                </p>
              </td>
            </tr>
            <tr>
              <td class="direction"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-of-tokyo-metro-oedo-line.png" alt="logo-of-tokyo-metro-oedo-line" title="logo-of-tokyo-metro-oedo-line"></td>
              <td>
                <p>
                  Toei Ōedo Line<br>
                  <span class="txt-grey">2 minutes from Roppongi / 11 minutes from Shinjuku</span>
                </p>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </section>

    <div class="desktop" style="height: 50px"></div>
    <div class="mobile" style="height: 30px"></div>
    <iframe width="100%" height="200%" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=coto%20japanese%20club&key=AIzaSyBIxbiVeKtrXly1YING3F_qS_ZC_NHz2AE" allowfullscreen></iframe>
    <div class="desktop" style="height: 30px"></div>
    <div class="mobile" style="height: 0px"></div>





    <section class="contact container max-720"><!-- map and metro map -->
      <img src="<?php bloginfo('stylesheet_directory'); ?>/img/tokyo-metro-subway-map-to-coto-japanese-club-azabu-juban.png" alt="tokyo-metro-subway-map-to-coto-japanese-club-azabu-juban" title="tokyo-metro-subway-map-to-coto-japanese-club-azabu-juban">
      <div class="desktop" style="height: 50px"></div>
      <div class="mobile" style="height: 0px"></div>
    </section>

    <div class="desktop" style="height: 20px"></div>
    <div class="mobile" style="height: 10px"></div>

    <section class="inquiry"><!-- Large inquiry form -->
      <img class="svg positive-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/positive-curve.svg">
      <div class="container max-780">
        <div class="text-centered">
          <h2 class="txt-velvet">Apply for one of our courses today!</h2>
          <p class="txt-dark-grey h5-size">Get in touch and let us know how we can help you achieve your study goals. We'll invite you over for a tour of the school and a free level check.</p>
          <div class="desktop" style="height: 40px"></div>
          <div class="mobile" style="height: 20px"></div>
        </div>
        <?php echo do_shortcode( '[contact-form-7 id="105" title="Large Inquiry Form"]' ); ?>
      </div>
    </section>

  </main>
</div>


<?php get_footer(); ?>
