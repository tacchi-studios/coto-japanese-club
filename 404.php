<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>
<div class="mobile" style="height: 40px"></div>
<div class="no-banner-curve desktop">
  <img class="positive-curve no-banner" src="<?php bloginfo('stylesheet_directory'); ?>/img/position-curve-no-banner.svg">
  <img class="curve-arrow-piece" src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-arrow-piece.svg">
</div>

<main>
  <div class="container max-780"><!-- title -->
    <div class="container text-centered">
      <h2>Oops! We couldn't find the page you were looking for.</h2>
      <div style="height:20px;"></div>
      <p class="txt-dark-grey h6-size f-weight-400">If you’re looking to study Japanese with us, get in touch anytime, or take a look at our study options below.</p>
      <div class="desktop" style="height: 20px"></div>
      <div class="mobile" style="height: 10px"></div>
    </div>
    <hr>
  </div>
  <div class="desktop" style="height: 50px"></div>
  <div class="mobile" style="height: 30px"></div>
  <section class="contact container max-780 no-pad"><!-- Contacts -->
    <div class="row" style="max-width: 600px; margin: 0 auto;">
      <div class="col-md-5">
        <table>
          <?php $page = get_page_by_title( 'Contact' ); ?><!-- get ID from page name -->
          <tr>
            <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
            <td><p><?php the_field('email_address', $page); ?></p></td>
          </tr>
          <tr class="phone">
            <td><i class="fa fa-mobile" aria-hidden="true"></i></td>
            <td><p><?php the_field('phone_number', $page); ?></p></td>
          </tr>
        </table>
      </div>
      <div class="col-md-7">
        <table>
          <tr>
            <td><i class="fa fa-building" aria-hidden="true"></i></td>
            <td><p><?php the_field('school_address', $page); ?><br><span class="txt-grey"><?php the_field('opening_hours', $page); ?></span></p></td>
          </tr>
        </table>
      </div>
    </div>
  </section>
  <div class="desktop" style="height: 20px"></div>
  <div class="mobile" style="height: 0px"></div>


  <section class="container max-1220"><!-- Courses -->
    <?php $loop = new WP_Query( array( 'post_type' => 'school_courses', 'posts_per_page' => 3 ) ); ?>
    <?php $the_count = $loop->found_posts; ?>
    <ul class="course-list flex-wrap">
      <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
        <?php get_template_part( 'loop-templates/course'); ?>
      <?php endwhile; wp_reset_query(); ?>
    </ul>
    <?php if ( ( $loop->have_posts() ) && ( $the_count > 3 ) ) : ?>
     <a class="more-link h5-size text-centered f-weight-400" href="<?php echo esc_url( home_url( '/courses' ) ); ?>">See all courses</a>
    <?php endif ?>
  </section>
  <div style="height:80px;"></div>
</main>




<?php get_footer(); ?>
