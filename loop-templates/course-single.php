<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

?>

<div class="banner sm">
  <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
  <div class="banner-img" style="background-image: url(<?php echo $backgroundImg[0]; ?>);"></div>
  <img class="desktop negative-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/banner-curve.svg">
  <img class="mobile negative-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/negative-curve-aqua.svg">
</div>

<div class="curve mobile bg-aqua">
  <img class="svg positive-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/positive-curve-mobile.svg">
</div>
<img class="curve-arrow-piece" src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-arrow-piece.svg">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
  <div style="height:20px;"></div>
  <div class="container max-840 blog-article course">
    <div class="text-centered">
      <h1 class="h2-size"><?php the_title(); ?></h1>
      <p class="h5-size"><?php the_field('course_description'); ?></p>
    </div>
    <div class="desktop" style="height:30px"></div>
    <div class="mobile" style="height:0px"></div>
    <div class="content">
      <div class="overview">
        <?php the_field('course_overview'); ?>
      </div>
      <?php the_field('course_detail'); ?>
    </div>
    <div class="desktop" style="height:120px;"></div>
    <div class="mobile" style="height:40px;"></div>
  </div>
</article><!-- #post-## -->

<section class="inquiry"><!-- Large inquiry form -->
  <img class="svg positive-curve" src="<?php bloginfo('stylesheet_directory'); ?>/img/positive-curve.svg">
  <div class="container max-780">
    <div class="text-centered">
      <h2 class="txt-velvet">Interested in our courses?</h2>
      <p class="txt-dark-grey h5-size">Get in touch and let us know how we can help you achieve your study goals. We'll invite you over for a tour of the school and a free level check.</p>
      <ul class="contact">
        <?php $page = get_page_by_title( 'Contact' ); ?><!-- get ID from page name -->
        <li><p><i class="fa fa-envelope" aria-hidden="true"></i><?php the_field('email_address', $page); ?></p><li>
        <li><p><i class="fa fa-mobile" aria-hidden="true"></i><?php the_field('phone_number', $page); ?></p><li>
      </ul>
    </div>
    <?php echo do_shortcode( '[contact-form-7 id="105" title="Large Inquiry Form"]' ); ?>
  </div>
</section>


