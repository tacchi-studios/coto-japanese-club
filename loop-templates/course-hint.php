<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

?>

<div class="col-md-4 col-xs-12 flex-wrap">
  <p class="h5-size hint"><i><?php the_field('course_hint'); ?></i></p>
</div>