<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

?>
<div class="no-banner-curve desktop">
  <img class="positive-curve no-banner" src="<?php bloginfo('stylesheet_directory'); ?>/img/position-curve-no-banner.svg">
  <img class="curve-arrow-piece" src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-arrow-piece.svg">
</div>

<div class="desktop" style="height: 60px"></div>
<div class="mobile" style="height: 80px"></div>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<div class="blog container max-840 blog-article">
	  <a href="<?php echo esc_url( home_url( '/blog' ) ); ?>"><p class="f-weight-400"><i class="fa fa-angle-double-left" aria-hidden="true"></i>Back to blog</p></a>
	  <div class="desktop" style="height:20px;"></div>
	  <div class="mobile" style="height:10px;"></div>
	  <p class="date f-weight-400"><?php echo get_the_date(); ?></p>
		<h1 class="h2-size"><?php the_title(); ?></h1>
	  <div class="desktop" style="height:30px;"></div>
	  <div class="mobile" style="height:15px;"></div>
    <?php if ( has_post_thumbnail() ) { ?>
			<?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
		  <div class="desktop" style="height:40px;"></div>
		  <div class="mobile" style="height:40px;"></div>
    <?php } ?>
		<div class="content">
		  <?php the_content(); ?>
		</div>
		<?php $post_object = get_field('include_event_call_to_action'); // Show selected event detail
		if( $post_object ):
			$post = $post_object;
			setup_postdata( $post );?>
	      <div class="desktop" style="height:30px;"></div>
	      <div class="mobile" style="height:15px;"></div>
		    <section class="events snippet"><!-- Show event detail -->
		      <p class="txt-aqua">event</p>
		      <ul>
		        <?php get_template_part( 'loop-templates/event'); ?>
		      </ul>
		    </section>
		   <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		<?php endif; ?>
	  <?php if( get_field('include_inquiry_form') == 'yes' ): ?>
  	  <div class="desktop" style="height:30px;"></div>
	    <div class="mobile" style="height:15px;"></div>
			<hr>
  	  <div class="desktop" style="height:30px;"></div>
	    <div class="mobile" style="height:15px;"></div>
	  	<h4>Interested in learning more about Japanese culture? Get in touch to hear about our courses!</h4>
  	  <div class="desktop" style="height:20px;"></div>
	    <div class="mobile" style="height:10px;"></div>
	    <?php echo do_shortcode( '[contact-form-7 id="173" title="Small inquiry form" html_class="small-inquiry-form"]' ); ?>
	  <?php endif; ?>
	  <div class="desktop" style="height:120px;"></div>
	  <div class="mobile" style="height:40px;"></div>
  </div>
</article><!-- #post-## -->





