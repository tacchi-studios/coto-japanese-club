<?php
/**
 * @package understrap
 */
?>
<li>
  <p class="h5-size hint"><i><?php the_field('course_hint'); ?></i></p>
  <a href="<?php the_permalink() ?>" class="course-item">
<!--     <p class="h5-size hint"><i><?php the_field('course_hint'); ?></i></p> -->
    <div class="banner course">
      <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );?>
      <div class="banner-img" style="background-image: url(<?php echo $backgroundImg[0]; ?>);"></div>
    </div>
    <div class="text-area">
      <h4><?php the_title(); ?></h4>
      <p class="h6-size"><?php the_field('course_description'); ?></p>
      <hr class="aqua short">
      <p class="highlights f-weight-400"><?php the_field('course_highlights'); ?></p>
    </div>
    <div class="learn-more" href="<?php the_permalink() ?>">Learn more</div>
  </a>
</li>
