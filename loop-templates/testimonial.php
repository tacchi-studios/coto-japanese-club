<?php
/**
 * @package understrap
 */
?>

<div class="item">
  <div class="avatar">
    <div class="banner">
      <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );?>
      <div class="banner-img" style="background-image: url(<?php echo $backgroundImg[0]; ?>);" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"></div>
    </div>
    <img src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-speech-bubble.svg" alt="speech-bubble">
  </div>
  <div style="height: 15px"></div>
  <p class="quote">"<?php the_field('quote'); ?>"</p>
  <div style="height: 15px"></div>
  <p><b><?php the_title(); ?></b> / <?php the_field('course_type'); ?></p>
</div>

