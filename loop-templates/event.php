<?php
/**
 * @package understrap
 */
?>
<li>
  <?php $id_post_original=get_the_ID();?>
  <h4><?php the_title(); ?></h4>
  <p class="txt-dark-grey h6-size description">
    <?php the_field('event_description'); ?>
    <span>
      <?php $post_object = get_field('link_to_blog_post');
      if( $post_object ):
        $post = $post_object;
        setup_postdata( $post );?>
           Learn more about the event from <a class="h6-size" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          <?php wp_reset_postdata(); ?>
      <?php endif; ?>
    </span>
  </p>

  <?php $post = $id_post_original; setup_postdata( $post ); ?>
  <ul>
    <li>
      <p class="txt-aqua"><b><i class="fa fa-calendar" aria-hidden="true"></i>Dates</b></p>
      <p class="f-weight-400"><?php the_field('event_date'); ?></p>
    </li>
    <li>
      <p class="txt-aqua"><b><i class="fa fa-map-pin" aria-hidden="true"></i>Venue</b></p>
      <p class="f-weight-400"><?php the_field('venue_address'); ?></p>
    </li>
    <li>
      <p class="txt-aqua"><b><i class="fa fa-tags" aria-hidden="true"></i>Price</b></p>
      <p class="f-weight-400"><?php the_field('price'); ?></p>
    </li>
    <li>
      <?php $page = get_page_by_title( 'Contact' ); ?><!-- get ID from page name -->
      <?php if( get_field('coubic_link') ) { ?>
        <p class="text f-weight-400">Sign up at coubic.com</p>
        <a target="_blank" href="<?php the_field('coubic_link'); ?>">
          <img src="<?php bloginfo('stylesheet_directory'); ?>/img/coubic-event-reserve-button.png" alt="coubic-event-reserve-button" title="coubic-event-reserve-button">
        </a>
        <div style="height:7px"></div>
        <p class="text p-size-sm">or email <a class="p-size-sm" href="mailto:<?php the_field('email_address', $page); ?>"><?php the_field('email_address', $page); ?></a></p>
      <?php } else { ?>
        <div class="mobile" style="height:20px"></div>
        <p class="email">
          Email <a href="mailto:<?php the_field('email_address', $page); ?>"><?php the_field('email_address', $page); ?></a>
          or inquire at the front desk to register
        </p>
      <?php } ?>
    </li>
  </ul>
  <div class="vertical-space" style="height:20px"></div>
  <hr>
  <div class="vertical-space" style="height:20px"></div>
</li>


