<?php
/**
 * Understrap functions and definitions
 *
 * @package understrap
 */

/**
 * Theme setup and custom theme supports.
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Load functions to secure your WP install.
 */
require get_template_directory() . '/inc/security.php';

/**
 * Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/enqueue.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/pagination.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/custom-comments.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load custom WordPress nav walker.
 */
require get_template_directory() . '/inc/bootstrap-wp-navwalker.php';

/**
 * Load WooCommerce functions.
 */
require get_template_directory() . '/inc/woocommerce.php';

/**
 * Load Editor functions.
 */
require get_template_directory() . '/inc/editor.php';

/**
* ***CUSTOM*** - Custom Pagination
*/
require get_template_directory() . '/inc/custom-pagination.php';


/********** CUSTOM FUNCTIONS FOR COTO CLUB **********/

// Replace login screen logo
function coto_club_logo() { ?>
  <style type="text/css">
    #login h1 a, .login h1 a {
      background-image: url(<?php bloginfo('stylesheet_directory'); ?>/img/logo.svg);
      height:130px;
      width:260px;
      background-size: 100%;
      background-repeat: no-repeat;
      padding-bottom: 10px;
    }
  </style>
<?php }
add_action( 'login_enqueue_scripts', 'coto_club_logo' );


// Rename "posts" label to to "blog"
function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Blogs';
    $submenu['edit.php'][5][0] = 'Blogs';
    $submenu['edit.php'][10][0] = 'Add blog';
    $submenu['edit.php'][16][0] = 'Blog tags';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Blog';
    $labels->singular_name = 'Blog';
    $labels->add_new = 'Add blog';
    $labels->add_new_item = 'Add blog';
    $labels->edit_item = 'Edit blog';
    $labels->new_item = 'Blog';
    $labels->view_item = 'View blog';
    $labels->search_items = 'Search blog';
    $labels->not_found = 'No blog found';
    $labels->not_found_in_trash = 'No blog found in Trash';
    $labels->all_items = 'All blogs';
    $labels->menu_name = 'Blog';
    $labels->name_admin_bar = 'Blog';
}

add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );

// Remove tags and categories support from posts
function myprefix_unregister_tags() {
    unregister_taxonomy_for_object_type('post_tag', 'post');
    unregister_taxonomy_for_object_type('category', 'post');
}
add_action('init', 'myprefix_unregister_tags');


// TinyMCE only allow paragraph and h4 for editor
function wpa_45815($arr){
    $arr['block_formats'] = 'Paragraph=p;Heading Title=h4;Small text=h6;Label=h5';
    return $arr;
  }
add_filter('tiny_mce_before_init', 'wpa_45815');

// TinyMCE adopt theme style to editor
function my_format_TinyMCE( $in ) {
    $in['content_css'] = get_template_directory_uri() . "/css/theme.min.css";
    return $in;
}
add_filter( 'tiny_mce_before_init', 'my_format_TinyMCE' );

// TinyMCE minor editor style changes
function content_editor_style(){ ?>
    <style type="text/css">
      .postarea.wp-editor-expand,
      #acf-course_overview,
      #acf-course_detail {
        max-width: 840px;
      }
    </style>
<?php } add_action( "admin_head", "content_editor_style" );

// TinyMCE only allow plain text paste
function change_paste_as_text($mceInit, $editor_id){
  $mceInit['paste_as_text'] = true;
  return $mceInit;
}

// // Custom pagination
// function wpbeginner_numeric_posts_nav() {
//   if( is_singular() )
//     return;
//   global $wp_query;
//   if( $wp_query->max_num_pages <= 1 )
//     return;
//   $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
//   $max   = intval( $wp_query->max_num_pages );
//   if ( $paged >= 1 )
//     $links[] = $paged;
//   if ( $paged >= 3 ) {
//     $links[] = $paged - 1;
//     $links[] = $paged - 2;
//   }
//   if ( ( $paged + 2 ) <= $max ) {
//     $links[] = $paged + 2;
//     $links[] = $paged + 1;
//   }
//   echo '<div class="navigation"><ul>' . "\n";
//   if ( get_previous_posts_link() )
//     printf( '<li>%s</li>' . "\n", get_previous_posts_link('<i class="fa fa-angle-double-left" aria-hidden="true"></i>') );
//   if ( ! in_array( 1, $links ) ) {
//     $class = 1 == $paged ? ' class="active"' : '';
//     printf( '<li%s><a href="%s">%s</a><span></span></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
//     if ( ! in_array( 2, $links ) )
//       echo '<li>…</li>';
//   }
//   sort( $links );
//   foreach ( (array) $links as $link ) {
//     $class = $paged == $link ? ' class="active"' : '';
//     printf( '<li%s><a href="%s">%s</a><span></span></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
//   }
//   if ( ! in_array( $max, $links ) ) {
//     if ( ! in_array( $max - 1, $links ) )
//       echo '<li>…</li>' . "\n";
//     $class = $paged == $max ? ' class="active"' : '';
//     printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
//   }
//   if ( get_next_posts_link() )
//     printf( '<li>%s</li>' . "\n", get_next_posts_link('<i class="fa fa-angle-double-right" aria-hidden="true"></i>') );
//   echo '</ul></div>' . "\n";
// }


// Disable settings menu for editor role access
function remove_menus(){
  $roles = wp_get_current_user()->roles;
  if( !in_array('editor',$roles)){
    return;
  }
  remove_menu_page( 'options-general.php' ); //Settings menu
}
add_action( 'admin_menu', 'remove_menus' , 100 );


// Disable other user roles when editor adds new user
remove_role('subscriber');
remove_role('author');
remove_role('contributor');
